install.packages("dplyr")
install.packages("forecast")
install.packages("tseries")
install.packages("Metrics")

library(dplyr)
library(forecast)
library(tseries)
library(Metrics)

##### READ DATA
crime_filepath  <- file.path('C:', 'Users', 'mzabo', 'Documents', 'PW', 'knsigolem', 'incidents_part1_part2_small.Rds')
weather_filepath  <- file.path('C:', 'Users', 'mzabo', 'Documents', 'PW', 'knsigolem', 'weather.csv')

df.crime <- readRDS(crime_filepath)
df.weather <- read.csv(weather_filepath, stringsAsFactors = FALSE, sep=',')


##### EXAMPLE ANALYSIS

# Decompose temperature
ts.temp <- ts(df.weather$TMAX ,frequency = 365, start=c(2010, 1, 1))
dec <- decompose(ts.temp)
plot(dec)

# Filter crime data
df.crime <- df.crime %>%
  select(dispatch_date, text_general_code)
